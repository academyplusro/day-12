package ORM;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Hotel {

	@Id
	private String location;
	private String name;

	public Hotel() {
	}

	public Hotel(String location, String name) {
		this.location = location;
		this.name = name;
	}
	
	public String getlocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Hotel [location=" + location + ", name=" + name + "]";
	}
}
