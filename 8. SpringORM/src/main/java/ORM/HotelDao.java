package ORM;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

@Component
public class HotelDao {

	@PersistenceContext
	private EntityManager em;

	public void persist(Hotel hotel) {
		em.persist(hotel);
	}

	public List<Hotel> findAll() {
		return em.createQuery("SELECT h FROM Hotel h").getResultList();
	}

}
