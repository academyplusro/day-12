package ORM;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class HotelService {

	@Autowired
	private HotelDao hotelDao;

	@Transactional
	public void add(Hotel hotel) {
		hotelDao.persist(hotel);
	}
	
	@Transactional
	public void addAll(Collection<Hotel> hotels) {
		for (Hotel hotel : hotels) {
			hotelDao.persist(hotel);
		}
	}

	@Transactional(readOnly = true)
	public List<Hotel> listAll() {
		return hotelDao.findAll();

	}
}
