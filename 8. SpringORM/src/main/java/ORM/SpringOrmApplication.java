package ORM;

import java.util.Arrays;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

public class SpringOrmApplication {

	public static void main(String[] args) {
				ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/bean.xml");
				
				HotelService hotelService = ctx.getBean(HotelService.class);
				
				
				hotelService.add(new Hotel("Bahamas","Kamalame Cay"));
				hotelService.add(new Hotel( "California", "Auberge du Soleil Rutherford"));
				
				System.out.println("listAll: " + hotelService.listAll());
				
				
				try {
					hotelService.addAll(Arrays.asList(new Hotel("Massachusetts", "Inn at Hastings Park")));
				} catch (DataAccessException dataAccessException) {
				}
				
				System.out.println("listAll: " + hotelService.listAll());
				
				ctx.close();
				
			}
	}

